<?php

class SuiteTest extends TestCase
{
    /**
     * @return \App\VenueFinder
     */
    protected function finder() : \App\VenueFinder
    {
        $venues = app()->make('venues');

        /** @var \App\VenueFinder $finder */
        $finder = new \App\VenueFinder($venues);

        return $finder;
    }

    public function testToAvoid()
    {
        $venues = app()->make('venues');

        $members = [
          [
              "name" => "John Davis",
            "wont_eat" => ["Fish"],
            "drinks" => ["Cider", "Rum", "Soft drinks"]
          ],
          [
              "name" => "Gary Jones",
            "wont_eat" => ["Eggs", "Pasta"],
            "drinks" => ["Tequila", "Soft drinks", "beer", "Coffee"]
          ]
        ];

        $members = json_decode(json_encode($members));

        $members = collect($members);

        /** @var \App\VenueFinder $finder */
        $finder = new \App\VenueFinder($venues);

        $result = $finder->toAvoidForMembers($members);

        $expected = [
            'Fabrique',
            'Twin Dynasty',
            'Wagamama',
            'Sultan Sofrasi',
            'Tally Joe'
        ];

        $this->assertEquals($expected, $result->keys()->toArray());
    }

    public function testToGo()
    {
        $venuesToAvoid = [
            "El Cantina" => 1,
            "Spirit House" => 1
        ];

        $venuesToAvoid = collect($venuesToAvoid);

        $finder = $this->finder();

        $result = $finder->toGo($venuesToAvoid);

        $expected = [
            'Twin Dynasty',
            'Spice of life',
            'The Cambridge',
            'Wagamama',
            'Sultan Sofrasi',
            'Tally Joe',
            'Fabrique'
        ];

        $this->assertEquals($expected, $result->pluck('name')->toArray());
    }
}
