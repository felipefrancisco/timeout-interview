<?php

namespace App;

use Illuminate\Support\Collection;

class VenueFinder
{
    /**
     * @var Collection
     */
    protected $venues;

    /**
     * VenueFinder constructor.
     * @param Collection $venues
     */
    public function __construct(Collection $venues)
    {
        $this->venues = $venues;
    }

    /**
     * @param Collection $toAvoid
     * @return Collection
     */
    public function toGo(Collection $toAvoid) : Collection
    {
        // Filtering venues to go according to the venues to avoid. After filtering, we're
        // plucking the name of the venues for display.
        $venuesToGo = $this->venues->filter(function($item) use ($toAvoid) {
            return !$toAvoid->has($item->name);
        });

        return $venuesToGo;
    }

    /**
     * @param Collection $members
     * @return Collection
     */
    public function toAvoidForMembers(Collection $members) : Collection
    {
        $venuesToAvoid = collect();

        // Since we're "Keeping it simple", we're going to simply run through the selected
        // members, checking if the venues available have something for them to eat and
        // drink. Venues that don't have anything available, will be added to the
        // venuesToAvoid variable with the reason why that member can't go to
        // the given place.
        foreach ($members as $member) {

            foreach ($this->venues as $venue) {

                // We're using the perssimist validation approach to keep low complexity.
                $avoidBecause = [];

                // If there's no difference between the available food and the things the member won't
                // eat, it means that there's nothing available for this person to eat in this
                // venue.
                if (!array_diff($venue->food, $member->wont_eat)) {

                    $avoidBecause[] = sprintf("There's nothing for %s to eat", $member->name);
                }

                // If there's no intersection between the available drinks and the drinks the member can
                // drink, it means that there's nothing available for this person do drink in this
                // venue.
                if (!array_intersect($venue->drinks, $member->drinks)) {

                    $avoidBecause[] = sprintf("There's nothing for %s to drink", $member->name);
                }

                // If no reasons to avoid this place were found, let's move on to the next.
                if (!count($avoidBecause)) {
                    continue;
                }

                // Adding the venue to the list if it hasn't been added before, setting a empty collection
                // of reasons that will be used in the next step.
                if (!$venuesToAvoid->has($venue->name)) {
                    $venuesToAvoid->put($venue->name, collect());
                }

                /** @var Collection $currentReasons */
                $currentReasons = $venuesToAvoid->get($venue->name);

                // Adding new reasons to previous found reasons.
                $allReasons = $currentReasons->merge($avoidBecause);

                // Adding all reasons to venue entry.
                $venuesToAvoid->put($venue->name, $allReasons);
            }
        }

        return $venuesToAvoid;
    }
}
