<?php

namespace App\Http\Controllers;

use App\VenueFinder;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\View\View;

class MainController extends Controller
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * MainController constructor.
     * @param Client $client
     */
    public function __construct(
        Client $client
    )
    {
        $this->client = $client;
    }

    /**
     * @return View
     */
    public function index() : View
    {
        return view('index', [
            'members' => app()->make('members')
        ]);
    }

    /**
     * @param Request $request
     * @param VenueFinder $finder
     * @return View
     * @throws \Exception
     */
    public function places(
        Request $request,
        VenueFinder $finder
    )
    {
        // Grab the selection from the request.
        $selected = $request->get('selected');

        if (!$selected || !is_array($selected)) {
            throw new \Exception('"selected" field is invalid');
        }

        $selected = collect($selected);

        // Fetching the members from the json-server and, since there's no database being
        // used, we're just going to filter the selected members by their names
        // according to the selection in the first screen.
        $members = app()->make('members')->filter(function($item) use ($selected) {
            return $selected->contains($item->name);
        });

        // Find places to avoid according to each members restrictions.
        $venuesToAvoid = $finder->toAvoidForMembers($members);

        // Filtering venues to go according to the venues to avoid. After filtering, we're
        // plucking the name of the venues for display.
        $venuesToGo = $finder->toGo($venuesToAvoid)->pluck('name');

        return view('places', [
            'toGo' => $venuesToGo,
            'toAvoid' => $venuesToAvoid
        ]);
    }
}
