### Considerations

This project uses `laravel/lumen` as micro-framework. 

### Requirements

- php >=7.1
- `npm`
- `composer`

### Getting Started

Run `npm` and `composer` install to download all dependencies.
```bash
npm install
composer install
```

In separate terminals windows, run the two commands below 
```bash
json-server --watch db/db.json
```
```bash
php -S localhost:8000 -t public
```

Go to `http://localhost:8000/` and check the application running.

### Running the tests

Simply run the following command on the project's root
```bash
./vendor/phpunit/phpunit/phpunit
```

### Comments

I was preparing a `docker-compose.yml` file with the necessary set up to bring the application up, however I realized that this might break the "KISS" proposed in the test description. However, I've left it in the project so you could see how I was thinking to use the mongo images to easily add the db to the application and seed it with the sample data provided.

The next step would be to incorporate the MongoDB connector to this project and make use of it instead of using `json-server`.

Since "KISS" was mentioned in the job descripition, I've left the whole algorithm inside the one method or two methods to keep it simple. If this wasn't a requirement, I would improve the structure of the `VenueFinder`, create `Venue` and `Member` classes, and probably a few more auxiliary classes like `InvalidVenueException`, `InvalidMemberException`, `PlacesRequest`, `ApiResourceClient`s, `Reason` and `ReasonEnumerator`s, for example.

Also, since it was said that UI wasn't the point of this project, I stuck to plain HTML, but would be happy to implement MD libraries for the interface. 